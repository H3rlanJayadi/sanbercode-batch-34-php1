1. Buat database 
CREATE DATABASE myshop;

2. membuat table di dalam databse
CREATE TABLE users( id int(8) PRIMARY KEY AUTO_INCREMENT, name varchar(255), email varchar(255), passwordd varchar(255) );
CREATE TABLE categori( id int(9)PRIMARY KEY AUTO_INCREMENT, name varchar(255) );
CREATE TABLE items( id int(8) PRIMARY KEY AUTO_INCREMENT, name varchar(255), description varchar(255), price int(9), stock int(8), categori_id int(9), FOREIGN KEY (categori_id) REFERENCES categori(id) );

3.memasukkan data pada table
A. user
INSERT INTO users(name) VALUES ("Jonh Doe"), ("Jane Doe");
INSERT INTO users(email) VALUES ("Jonh@Doe.com"), ("Jane@Doe.com");
INSERT INTO users(passwordd) VALUES ("Jonh123"), ("Jenita123");
B. categori
INSERT INTO categori(name) VALUES ("gadget"),("cloud"),("men"),("women"),("brended");
C. items
INSERT INTO items (name, description, price, stock, categori_id) VALUES ("Sumsang B50","hape keren dari merek sumsang",4000000, 100, 1);
INSERT INTO items (name, description, price, stock, categori_id) VALUES ("Unikloh","baju keren dari brand ternama",500000, 50, 2);
INSERT INTO items (name, description, price, stock, categori_id) VALUES ("IMHO Watch","Jam tangan anak yang jujur banget",2000000, 10, 1);

4.mengambil data dari table
A. mengambil Data Users
SELECT id, name, email FROM users;
B.mengambil data item
SELECT * FROM `items` WHERE price >1000000;
SELECT * FROM `items` WHERE name LIKE "watch&";
C.menampilkan data items join 
SELECT items.name, items.description, items.price, items.stock, items.categori_id, categori.name AS kategori FROM items INNER JOIN categori ON items.categori_id = categori.id;

5.mengubah items data 
UPDATE items SET price = 25000000 WHERE id=5;